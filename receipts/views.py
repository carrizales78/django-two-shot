from django.shortcuts import redirect, render
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()

    categories = ExpenseCategory.objects.all()
    accounts = Account.objects.all()

    context = {
        'form': form,
        'categories': categories,
        'accounts': accounts,
    }

    return render(request, 'receipts/create_receipt.html', context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    return render(request, 'categories/category_list.html', {'categories': categories})


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    return render(request, 'accounts/account_list.html', {'accounts': accounts})

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()

    return render(request, 'categories/create_category.html', {'form': form})


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()

    return render(request, 'accounts/create_account.html', {'form': form})
